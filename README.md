# dddspecification

#### 介绍
DDD规范定义, 此规范描述了使用DDD会用到的相关组件，而不是描述DDD的代码结果。
DDD代码结构根据不同人的理解，会有不同的代码结果规范。
下面是基于本人的一些理解，整理的DDD代码结构， 如下：

 -application <br>
 +clients <br>
 &nbsp;&nbsp;&nbsp;&nbsp; -api <br>
 &nbsp;&nbsp;&nbsp;&nbsp; -controller <br>
 -infrastructure <br>
 -domains <br>
 &nbsp;&nbsp;&nbsp;&nbsp; +xxxdomain <br>
 &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  -entity <br>
 &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  -event <br>
 &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  -eventhandle <br>
 &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  -factory <br>
 &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  -repository <br>
 &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  -service <br>
 &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  -spi <br> 
 &nbsp;&nbsp;&nbsp;&nbsp; +xxxdomain <br>
 &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  -entity <br>
 &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  -event <br>
 &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  -eventhandle <br>
 &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  -factory <br>
 &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  -repository <br>
 &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  -service <br>
 &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  -spi <br>  

#### 规范说明
- application 应用服务定义的地方，我更喜欢把应用服务称作为创新服务
- client 应用接口层，应用服务和领域服务都可以通过client层，将服务暴露出去。 应用服务暴露出去的接口称为控制层（controller）， 
  领域服务暴露出去的接口称为api层（api）
- infrastructure 应用层和领域层都会用到的基础架构层，基础架构的具体实现
- domains 领域层，一个应用可以有多个领域
- xxxdomain 具体的领域层
- xxxdomain.entity 领域模型
- xxxdomain.event 领域事件
- xxxdomain.eventhandle 领域对外部事件的响应
- xxxdomain.factory 为领域模型服务的工厂类
- xxxdomain.repository 领域层会用到的领域模型持久化
- xxxdomain.service  领域服务
- xxxdomain.spi 当前领域对其他领域对象的依赖


