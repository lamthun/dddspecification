package com.lamthun.ddd.factory;

import com.lamthun.ddd.entity.Entity;
import com.lamthun.ddd.entity.Entity;

/**
 * 对领域工厂的描述, 通常一个领域entity由领域工厂来创建.
 *
 * @author lilei
 */
public interface DomainFactory<T extends Entity> {

    T create();
}
