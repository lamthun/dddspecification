package com.lamthun.ddd.client.request;

import java.io.Serializable;

/**
 * 对排序进行定义, 指定列以及指定排序规则
 *
 * @author lilei
 */
public class OrderDesc implements Serializable {
    private static final long serialVersionUID = -1L;
    private String col;
    private boolean asc = true;

    public String getCol() {
        return col;
    }

    public void setCol(String col) {
        this.col = col;
    }

    public boolean isAsc() {
        return asc;
    }

    public void setAsc(boolean asc) {
        this.asc = asc;
    }

}
