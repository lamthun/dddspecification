package com.lamthun.ddd.client.request;

import com.lamthun.ddd.client.DTO;

/**
 * 对查询式请求进行定义, 基于CQRS思想
 *
 * @author lilei
 */
public abstract class QueryRequest extends DTO {
}
