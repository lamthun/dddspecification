package com.lamthun.ddd.client.request;

import com.lamthun.ddd.client.DTO;
import com.lamthun.ddd.client.DTO;

/**
 * 对命令式请求的定义, 基于CQRS思想
 *
 * @author lilei
 */
public abstract class CommandRequest extends DTO {
}
