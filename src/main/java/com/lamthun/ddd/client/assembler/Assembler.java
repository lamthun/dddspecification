package com.lamthun.ddd.client.assembler;

import com.lamthun.ddd.client.DTO;
import com.lamthun.ddd.entity.Entity;
import com.lamthun.ddd.entity.Entity;

/**
 * 对装配的定义,
 * 装配指的是将request转化为entity, 将entity转化为response等, 且不仅仅限于此的装配.
 *
 * @author lilei
 */
public interface Assembler {
    DTO assembleDTO(Entity entity);

    Entity assembleEntity(DTO dto);
}
