package com.lamthun.ddd.client.response;

import com.lamthun.ddd.client.DTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 对返回数据是一个列表情况的定义, 返回的标准格式如下:
 * {
 * "responseCode" : "1000",
 * "responseMessage" : "ok",
 * "data" :
 * [
 * {instance of dto},
 * {instance of dto}
 * ]
 * }
 *
 * @author lilei
 */
public class MultiResponse<T extends DTO> extends Response {
    private int total;

    private Collection<T> data;

    public int getTotal() {
        return total;
    }


    public void setTotal(int total) {
        this.total = total;
    }

    public List<T> getData() {
        return null == data ? new ArrayList<>() : new ArrayList<>(data);
    }


    public void setData(Collection<T> data) {
        this.data = data;
    }

    public static <T extends DTO> MultiResponse<T> buildResponseWithTotal(int total, String responseCode, String responseMessage, Collection<T> data) {
        MultiResponse<T> response = new MultiResponse<>();
        response.setTotal(total);
        response.setResponseCode(responseCode);
        response.setResponseMessage(responseMessage);
        response.setData(data);
        return response;
    }
}
