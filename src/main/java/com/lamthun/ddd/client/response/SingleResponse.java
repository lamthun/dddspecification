package com.lamthun.ddd.client.response;

import com.lamthun.ddd.client.DTO;

/**
 * 具体单一返回对象的定义, 对外输出的格式标准如下:
 * {
 * "responseCode" : "1000",
 * "responseMessage" : "ok",
 * "data" : {instance of dto}
 * }
 *
 * @author lilei
 */
public class SingleResponse<T extends DTO> extends Response {
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static <T extends DTO> SingleResponse<T> buildResponse(String responseCode, String responseMessage, T data) {
        SingleResponse<T> response = new SingleResponse<>();
        response.setResponseCode(responseCode);
        response.setResponseMessage(responseMessage);
        response.setData(data);
        return response;
    }
}
