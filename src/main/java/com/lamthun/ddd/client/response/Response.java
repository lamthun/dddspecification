package com.lamthun.ddd.client.response;

import com.lamthun.ddd.client.DTO;

/**
 * 对返回的定义, 这里一般约定一个返回代码和返回message
 *
 * @author lilei
 */
public class Response extends DTO {
    private static final long serialVersionUID = -1L;

    private String responseCode;

    private String responseMessage;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    @Override
    public String toString() {
        return "Response [responseCode=" + responseCode + ", responseMessage=" + responseMessage + "]";
    }

    public static Response buildResponse(String responseCode, String responseMessage) {
        Response response = new Response();
        response.setResponseCode(responseCode);
        response.setResponseMessage(responseMessage);
        return response;
    }
}
