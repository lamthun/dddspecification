package com.lamthun.ddd.client;

import java.io.Serializable;

/**
 * 对数据传输对象(Data Transfer object)的定义
 *
 * @author lilei
 */
public abstract class DTO implements Serializable {
    private static final long serialVersionUID = -1L;
}
