package com.lamthun.ddd.repository;

import com.lamthun.ddd.entity.Entity;

/**
 * 基于CQRS思路设计的仓库存储查询执行器,方便后期做读写分离的优化
 * 主要处理新增,修改,删除等等数据命令
 *
 * @author lilei
 */
public interface RepositoryCommander<T extends Entity> extends Repository {
}
