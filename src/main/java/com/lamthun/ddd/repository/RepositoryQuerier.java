package com.lamthun.ddd.repository;

import com.lamthun.ddd.entity.Entity;

/**
 * 基于CQRS思路设计的仓库存储查询执行器,方便后期做读写分离的优化
 * 主要处理查询命令
 *
 * @author lilei
 */
public interface RepositoryQuerier<T extends Entity> extends Repository {
}
