package com.lamthun.ddd.spi;

/**
 * 定义了对其他领域服务的依赖， 一般通过RPC的方式请求其他领域服务
 *
 * @author lilei
 */
public interface SpiService {
}
