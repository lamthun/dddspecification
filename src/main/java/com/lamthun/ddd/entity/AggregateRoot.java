package com.lamthun.ddd.entity;

import com.lamthun.ddd.entity.Entity;

/**
 * 对领域聚合根的定义,聚合根对外部对象来说,就是一个实体.
 * 但是对内部来说, 聚合根是对不同实体的聚合关系(可以用uml类图体现)
 *
 * @author lilei
 */
public interface AggregateRoot extends Entity {
}
