package com.lamthun.ddd.event;

/**
 * 事件发布器
 *
 * @author lilei
 */
public interface EventPublisher {
    void publish(Event event);
}
