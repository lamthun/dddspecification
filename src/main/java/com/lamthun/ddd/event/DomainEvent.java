package com.lamthun.ddd.event;

public class DomainEvent extends Event {
    /**
     * 领域名称
     */
    String domainName;

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    @Override
    public String toString() {
        return "DomainEvent{" +
                "domainName='" + domainName + '\'' +
                ", eventId='" + eventId + '\'' +
                ", timestamp=" + timestamp +
                ", eventData='" + eventData + '\'' +
                '}';
    }
}
