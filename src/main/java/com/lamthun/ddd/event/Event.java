package com.lamthun.ddd.event;

import java.io.Serializable;
import java.util.Date;

/**
 * 对事件的定义
 *
 * @author lilei
 */
public class Event implements Serializable {
    private static final long serialVersionUID = -1L;

    /**
     * 事件ID
     */
    String eventId;

    /**
     * 事件时间
     */
    Date timestamp;

    /**
     * 事件数据, 建议这里使用json表达
     */
    String eventData;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getEventData() {
        return eventData;
    }

    public void setEventData(String eventData) {
        this.eventData = eventData;
    }

    @Override
    public String toString() {
        return "Event{" +
                "eventId='" + eventId + '\'' +
                ", timestamp=" + timestamp +
                ", eventData='" + eventData + '\'' +
                '}';
    }
}
